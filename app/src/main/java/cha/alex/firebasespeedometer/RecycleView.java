package cha.alex.firebasespeedometer;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class RecycleView extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<String> listData;

    DatabaseReference fbReference;

    public RecycleView() {
        fbReference = FirebaseDatabase.getInstance().getReference("message");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_view);

        recyclerView =  findViewById(R.id.recyclers_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        listData = new ArrayList<>();
        adapter = new MyAdapter(listData);
        recyclerView.setAdapter(adapter);


        fbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listData.clear();
                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    listData.add(ds.getValue()+"");
                }
                adapter.notifyDataSetChanged();
            }
        });

//        Collections.reverse(listData);
    }

    public void  sendDb(DatabaseReference firebaseReference) {
        fbReference = firebaseReference;
    }


    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<String> listData;

        class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            TextView mTextView;
            ViewHolder(TextView v) {
                super(v);
                v.setTextColor(Color.parseColor("#e17055"));
                v.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                mTextView = v;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        MyAdapter(ArrayList<String> list) {
            listData = list;
        }

        // Create new views (invoked by the layout manager)
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // create a new view
            TextView v = (TextView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
            return new ViewHolder(v);
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.mTextView.setText(listData.get(position));
        }

        @Override
        public int getItemCount() {
            return listData.size();
        }
    }
}
