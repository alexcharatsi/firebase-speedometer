package cha.alex.firebasespeedometer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements LocationListener{

    Toolbar myToolbar;

    LocationManager locationManager;
    static final int REQ_CODE = 123;
    int state;

    Button button;
    DecimalFormat df;
    AwesomeSpeedometer speedometer;

    DatabaseReference firebaseReference;

    String limit;
    int colorSelected;
    boolean first, overpassed;
    Double lat = null;
    Double lng = null;
    String lastLat;
    String lastLng;

    RecycleView recycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set Toolbar with 3 dots as an Options Menu
        myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.inflateMenu(R.menu.appbar_tools);

        speedometer = findViewById(R.id.awesomeSpeedometer);

        Intent intent = getIntent();
        //is it ever not null???
        if(intent != null) {
            limit = getIntent().getStringExtra("limit");
        }
        if(limit == null)
            limit = "50";

        Toast.makeText(this, "LIMITTO " + limit, Toast.LENGTH_SHORT).show();

        firebaseReference = FirebaseDatabase.getInstance().getReference("message");
        recycleView = new RecycleView();

        button = findViewById(R.id.button);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        first = true;
        lat = null;
        lng = null;

        state = 0;
        colorSelected = 0;
        first = false;
        overpassed = false;
        df = new DecimalFormat("###.#######");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_tools, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.putExtra("state", state);
                startActivity(intent);
                return true;

            case R.id.limitLog:
                recycleView.sendDb(firebaseReference);
                startActivity(new Intent(this, RecycleView.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onGpsClick(View view)   {
        if (state == 0) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, REQ_CODE);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                button.setText(R.string.stopButton);
            }
            state = 1;
        }
        else if (state == 1){
            gpsOff(view);
            state = 0;
            button.setText(R.string.startButton);
        }
    }

    //TODO na allaksw tis times px 50, 5, 100

    public void gpsOff(View view){
        locationManager.removeUpdates(this);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onLocationChanged(Location location) {
        float s;

        if(lat != null && lng != null) {
            lastLat = df.format(lat.doubleValue());
            lastLng = df.format(lng.doubleValue());
        }
        lat = location.getLatitude();
        lng = location.getLongitude();
        s = (location.getSpeed()*3600)/1000;
        String latitude = df.format(lat);
        String longitude = df.format(lng);
        if(latitude.equals(lastLat) && longitude.equals(lastLng)){
            s = 0;
        }

        if(s > Float.parseFloat(limit) && !overpassed){
            firebaseReference.push().setValue(limit + " exceeded: "+ new SimpleDateFormat("dd-MM-yyyy HH:mm").format(Calendar.getInstance().getTime()));
            overpassed = true;
        }
        if(s < Float.parseFloat(limit) && overpassed) {
            overpassed = false;
        }
        speedometer.speedTo(s);
        Toast.makeText(this, s + " speed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}