package cha.alex.firebasespeedometer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner speedLimitDropdown;

    String limit = "50";
    Button save;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        save = findViewById(R.id.buttonSave);
        save.setOnClickListener(this);

        speedLimitDropdown = findViewById(R.id.SpeedLimitSpinner);

        ArrayAdapter<CharSequence> limitAdapter = ArrayAdapter.createFromResource(this, R.array.maxLimit, R.layout.support_simple_spinner_dropdown_item);

        limitAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        speedLimitDropdown.setAdapter(limitAdapter);

        speedLimitDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        limit = "50";
                        break;
                    case 1:
//                        limit = "5";
                        limit = "80";
                        break;
                    case 2:
                        limit = "100";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Do nothing!
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("limit", limit);
        startActivity(intent);
    }
}
